#include <iostream>

#include "Singleton.h"

int main()
{
    auto singleton1 = Singleton::getInstance();
    auto singleton2 = Singleton::getInstance();

    std::cout << "singleton1 address: " << singleton1 << std::endl;
    std::cout << "singleton2 address: " << singleton2 << std::endl;

    return 0;
}