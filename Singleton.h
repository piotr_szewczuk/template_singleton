//
// Created by Piotr Szewczuk on 13/10/2018.
//

#ifndef SINGLETON_SINGLETON_H
#define SINGLETON_SINGLETON_H

#include <memory>

class Singleton
{
public:
    static std::shared_ptr<Singleton> getInstance();

private:
    Singleton() = default;
//    Singleton(const Singleton&) = delete;
//    Singleton operator==(const Singleton&) = delete;
    static std::shared_ptr<Singleton> instance;
};


#endif //SINGLETON_SINGLETON_H
