//
// Created by Piotr Szewczuk on 13/10/2018.
//

#include "Singleton.h"

std::shared_ptr<Singleton> Singleton::instance = nullptr;


std::shared_ptr<Singleton> Singleton::getInstance()
{
    if (!instance)
    {
        instance =  std::shared_ptr<Singleton>(new Singleton);
    }
    return instance;
}
